import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default ({ mode }) => {
  require('dotenv').config({ path: `./.env.${mode}` });
  return defineConfig({
    server: {
      // 是否自动打开浏览器
      open: true,
      // 服务器主机名，如果允许外部访问，可设置为"0.0.0.0"
      host: '0.0.0.0',
      // 服务器端口号
      port: 3000,
      // 设为 true ,若端口已被占用则会直接退出，而不是尝试下一个可用端口
      strictPort: false,
      // 为开发服务器配置 CORS
      cors: true,
      // 设置为 true 强制使依赖预构建
      force: true,
      // 代理
      proxy: {
        '/api':
        {
          target: process.env.VITE_APP_PROXY_URL,
          changeOrigin: true,
          rewrite:
            (path) => path.replace(/^\/api/, '')
        }
      }
    }
    ,
    // 将要用到的插件数组
    plugins:
      [
        vue()
      ]
  })
}

