export let credentialsTypes = [
    {
      id: 1,
      name: "一寸",
      width: 295,
      height: 413,
      minBytes: 50,
      maxBytes: 500,
      backgroundColor: "蓝色",
    },
    {
      id: 2,
      name: "大一寸",
      width: 390,
      height: 567,
      minBytes: 50,
      maxBytes: 500,
      backgroundColor: "蓝色",
    },
    {
      id: 3,
      name: "小二寸",
      width: 413,
      height: 531,
      minBytes: 50,
      maxBytes: 500,
      backgroundColor: "蓝色",
    },
    {
      id: 4,
      name: "二寸",
      width: 413,
      height: 579,
      minBytes: 50,
      maxBytes: 500,
      backgroundColor: "蓝色",
    },
    {
      id: 5,
      name: "小二寸",
      width: 413,
      height: 531,
      minBytes: 50,
      maxBytes: 500,
      backgroundColor: "蓝色",
    },
    {
      id: 6,
      name: "驾驶证",
      width: 260,
      height: 378,
      minBytes: 14,
      maxBytes: 30,
      backgroundColor: "白色",
    },
    {
      id: 7,
      name: "社保证",
      width: 358,
      height: 441,
      minBytes: 10,
      maxBytes: 20,
      backgroundColor: "白色",
    },
    {
      id: 8,
      name: "身份证",
      width: 358,
      height: 441,
      minBytes: 20,
      maxBytes: 500,
      backgroundColor: "白色",
    },
    {
      id: 9,
      name: "教师资格证",
      width: 295,
      height: 413,
      minBytes: 20,
      maxBytes: 500,
      backgroundColor: "白底",
    },
    {
      id: 10,
      name: "护照",
      width: 390,
      height: 567,
      minBytes: 20,
      maxBytes: 500,
      backgroundColor: "白底",
    },

    {
      id: 11,
      name: "四六级/计算机",
      width: 144,
      height: 192,
      minBytes: 20,
      maxBytes: 200,
      backgroundColor: "白底",
    },
    {
      id: 12,
      name: "司法考试",
      width: 413,
      height: 626,
      minBytes: 50,
      maxBytes: 800,
      backgroundColor: "白底",
    },
    {
      id: 13,
      name: "会计",
      width: 144,
      height: 156,
      minBytes: 20,
      maxBytes: 500,
      backgroundColor: "白底",
    },
    {
      id: 14,
      name: "护士",
      width: 160,
      height: 210,
      minBytes: 20,
      maxBytes: 500,
      backgroundColor: "白底",
    },
    {
      id: 15,
      name: "普通话",
      width: 413,
      height: 579,
      minBytes: 20,
      maxBytes: 500,
      backgroundColor: "白底",
    },
    {
      id: 16,
      name: "高考",
      width: 480,
      height: 640,
      minBytes: 20,
      maxBytes: 500,
      backgroundColor: "白底",
    },
    {
      id: 0,
      name: "自定义格式",
      width: 100,
      height: 100,
      minBytes: 10,
      maxBytes: 500,
      backgroundColor: "白色",
    },
  ]