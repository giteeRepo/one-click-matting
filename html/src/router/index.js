import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  {
    path: '/',
    component: () => import('../views/cropper/cropper.vue'),
    hidden: true
  },
  {
    path: '/demo',
    component: () => import('../views/cropper/compress.vue'),
    hidden: true
  }
]
const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
