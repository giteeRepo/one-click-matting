import request from '/src/utils/request'
  
// 品牌列表
export function removebg(formData) {
  return request({
    url: '/api/removebg',
    method: 'post',
    data: formData,
  })
}
